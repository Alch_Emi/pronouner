Activity::playing(
    concat!(
        'v',
        env!("CARGO_PKG_VERSION"),
        " | !pronouns",
    )
)

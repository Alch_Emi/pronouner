**Command Usage:** `!pronouns [add] [modifier] <base>`

Use this command to add your pronouns to yourself.  If you want to add
Sie/Hir/Hirs pronouns to yourself (assuming they exist in your server already),
you can run `!pronouns add Sie/Hir/Hirs`, or just `!pronouns sie`.  You can also
stick a modifier in front of your pronouns to add that to your role, e.g.
`!pronouns {sample_modifier} sie/hir`

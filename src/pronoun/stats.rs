// Copyright 2019 Emi Simpson <emi@alchemi.dev>

// This file is part of the Pronouner project.
//
// The Pronouner project is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// The Pronouner project is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
// for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with the Pronouner project.  If not, see <https://www.gnu.org/licenses/>.
use itertools::*;
use serenity::model::prelude::*;

use std::collections::{
    hash_map::*,
    HashSet,
};
use std::convert::TryInto;

use crate::pronoun::*;

/// A struct of usage information for a set of pronouns
///
/// The lifetime of this is bound to the database it was created with not just
/// because of a borrow, but also because, much like the [`PronounDatabase`],
/// this should not be kept persistantly, as it will not update as roles change.
/// Instead, throw it away when you're done with it.
///
/// *Disclaimer: Please do not apply this strategy to physical things.  That's
/// bad for the environment.  There is no borrow checker to magic away your used
/// bottles*
///
/// This may be used for generating neat stats for the end user, or, as is most
/// often the case, in determining if a set of roles can be safely pruned.
#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct PronounStats {

    /// How many users have each role
    ///
    /// For example, if three people used the "Trying She/Her/Hers" role, the
    /// corresponding [`RoleId`] would have 3 listed.  This includes roles that aren't
    /// managed by the bot!
    user_counts: HashMap<RoleId, u32>,

    /// How many [`PronounRole`]s exist for each pronoun base.
    ///
    /// For example, if at least 3 people had "Trying She/Her/Hers" and one person had
    /// "She/Her/Hers", then "She/Her/Hers" would have 2 listed
    role_counts: HashMap<String, u8>,
}

impl PronounStats {

    /// Calculates the stats for a given guild
    ///
    /// This stores the exact count of members on each role in the database.  If
    /// given a database that doesn't align with this guild, the resulting Stats
    /// will be empty, so it's recomended that the database and the guild align.
    /// This also assumes that the given guild has all of it's members cached.
    /// If not, any members not in the guild's cache will be ignored.
    ///
    /// **See Also:** [`PronounDatabase::calc_stats`]
    pub fn calculate(
        guild: &Guild,
        database: &PronounDatabase,
    ) -> Self {
        let mut user_counts = HashMap::with_capacity(database.len());

        database.list_roles()
            .for_each(|r|{
                user_counts.insert(r.role.id, 0);
            });

        guild.members
            .values()
            .flat_map(|m| m.roles.iter())
            .cloned()
            .for_each(|r| {
                user_counts.entry(r).and_modify(|c| *c += 1);
            });

        let role_counts = database.iter()
            .map(|entry| (
                entry.0.to_string(),
                entry.1
                    .len()
                    .try_into()
                    .expect("PronounDB with too many roles!")
            ))
            .collect::<HashMap<String, u8, _>>();

        Self { user_counts, role_counts }
    }

    /// Determines if a given role is eligable to be pruned.
    ///
    /// The current rules for pruning are that at least one role must exist per
    /// [`PronounSet`] at any given time (with the exception of special
    /// pronouns, which need no roles), and a role can only be pruned if it has
    /// no users, meaning if a role is taken out no one must lose their
    /// pronouns, and at least one role remains in the set.
    ///
    /// As a result, two vectors are produced, the left one containing all of
    /// the prunable roles, and the left (position zero) with all of the roles
    /// that should not be pruned.
    ///
    /// After return, this stats object is updated to appear as if the roles
    /// returned for deleting have been deleted, meaning that if you don't
    /// delete these roles, the statistics will no longer be accurate, and
    /// you'll need to calculate a new statistics object.  Alternatively, clone
    /// the statistics before hand.
    ///
    /// In the event that one of two pronouns can be pruned, priority is given to
    /// the pronoun that is not a base role, as base roles are considered better
    /// to leave unpruned.
    ///
    /// In some cases, it may be necessary to check if a role can be pruned
    /// while one user still has it.  This can be done to avoid an extra HTTP
    /// request (i.e. why remove the role then delete it when you can just
    /// delete it).  In this case, set `allow_one` to true to allow prunning
    /// roles with a single user.
    ///
    /// # Panics
    /// Panics if one of the given roles isn't present in the database the
    /// `PronounStats` was constructed with
    pub fn can_prune(
        &mut self,
        roles: impl IntoIterator<Item = PronounRole>,
        specials: &[String],
        allow_one: bool,
    ) -> (Vec<PronounRole>, Vec<PronounRole>) {

        let specials: HashSet<&str> = specials.iter()
            .map(String::as_str)
            .collect();

        // Put the base roles last
        // Replace this with partition in place once that comes out (#62543)
        let (modified_roles, base_roles): (Vec<_>, Vec<_>) = roles.into_iter()
            .partition_map(
                |role| if role.modifier().is_some() {
                    Either::Left(role)
                } else {
                    Either::Right(role)
                }
            );
        let roles = modified_roles.into_iter().chain(base_roles);

        let mut prune_roles = Vec::new();
        let mut noprune_roles = Vec::new();

        for role in roles {
            if
                self.user_counts.contains_key(&role.role.id) &&
                (self.user_counts[&role.role.id] <= allow_one as u32) &&
                (
                    *self.role_counts.get(role.base()).unwrap_or(&0) > 1 ||
                    specials.contains(role.base())
                )
            {
                *self.role_counts.get_mut(role.base()).unwrap() -= 1;
                self.user_counts.remove(&role.role.id);
                prune_roles.push(role);
            } else {
                noprune_roles.push(role);
            }
        }

        (prune_roles, noprune_roles)
    }

    /// Gets a count of the number of users per role
    ///
    /// Each role considered in the stats is represented by a [`RoleId`], which
    /// links to an integer designating the number of members with that role.
    ///
    /// The roles will be looked up using the given pronoun database.
    pub fn user_counts(
        &self,
        db: &PronounDatabase
    ) -> HashMap<PronounRole, u32> {
        db.list_roles()
            .filter_map(
                |r| self.user_counts
                    .get(&r.role.id)
                    .map(|c| (r, *c))
            )
            .collect()
    }

    /// Gets a count of the number of users for a given [`PronounSet`]
    ///
    /// This counts up all users using any variant of this pronoun set.  For example, if
    /// one person used "Trying She/Her/Hers", two people used "She/Her/Hers", and one
    /// person used "Not She/Her/Hers", than this method would return 4 for the set
    /// "She/Her/Hers"
    pub fn user_counts_for_set(
        &self,
        set: &PronounSet
    ) -> u32 {
        set.values()
           .filter_map(|r| self.user_counts.get(&r.id))
           .sum()
    }

}

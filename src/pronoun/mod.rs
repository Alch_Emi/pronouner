// Copyright 2019 Emi Simpson <emi@alchemi.dev>

// This file is part of the Pronouner project.
//
// The Pronouner project is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// The Pronouner project is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
// for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with the Pronouner project.  If not, see <https://www.gnu.org/licenses/>.

//! The lowest level structure is a `PronounRole`, which represents a single
//! `Role` in a Discord.
//!
//! This includes both a `Modifier`, like ("Not", or "Trying"), as well as a
//! base, like ("Fae/Faer/Faers").
//!
//! A collection of zero or more `PronounRole`s with the same base can be
//! represented with a `PronounSet`, and a collection of `PronounSet`s in
//! turn becomes a `PronounDatabase`.
use lazy_static::lazy_static;

use regex::Regex;

use serenity::model::{
    guild::Role,
    id::RoleId,
};

use futures::Future;

use std::{
    borrow::Borrow,
    collections::hash_map::HashMap,
    fmt,
    iter::FromIterator,
    ops::{
        Deref,
        Range,
    },
};

mod stats;
pub use stats::PronounStats;

lazy_static! {
    static ref PRONOUN_REGEX: Regex = Regex::new(
        r"^(?:(?P<modifier>.+)\s+)?(?P<base>[A-Z][a-z']*/[A-Z][a-z']*/[A-Z][a-z']*)$"
    ).unwrap();
}

/// A short hand for the modifier preceding a pronoun.
///
/// This is usually a single word that describes how the pronoun is or isn't
/// used.  For example in the pronoun "Trying Ae/Aer/Aers", the modifier would
/// be `Some("Trying")`.  In pronouns without a modifier, like just
/// "Ae/Aer/Aers", this is `None`.  This shorthand typedef is provided since very
/// many methods take a modifier like this, and it's not always clear what
/// `Option<String>` means.
pub type Modifier = Option<String>;

/// A struct representing a Discord role in turn representing users' pronouns.
///
/// This role has two parts, a base, which corresponds to the actual pronouns
/// that appear in the role (e.g. She/Her/Hers or Xe/Xem/Xyr), and the modifier,
/// which is some word preceding the base, indicating how it should be used.
/// For example, with the modifier "Not" and the base "He/Him/His", the role
/// ought to be named "Not He/Him/His", and would indicate that this user does
/// not use He/Him/His pronouns.
///
/// Roles that have not yet been created, and thus do not actually exist within
/// a Discord server, should not be represented by a PronounRole.  Instead,
/// consider a tuple ([`PronounSet`], [`Modifier`]), or ([`String`],
/// [`Modifier`]), depending on if any pronouns of the same base exist, and how
/// you plan on using it.
#[derive(Clone, Debug, Eq)]
pub struct PronounRole {
    pub role: Role,
    base: Range<usize>, //A slice of `role.name`, e.g. She/Her/Hers
    modifier: Option<Range<usize>>, //A slice of `role.name`, e.g. Trying or Not
}

impl PronounRole {
    /// Returns a reference to the base pronouns.
    ///
    /// # Example
    /// ```
    /// # use serde_yaml::from_str;
    /// # use serenity::model::guild::Role;
    /// # use libpronouner::pronoun::PronounRole;
    /// // Assuming role has already been defined.
    /// # let role: Role
    /// # = from_str(include_str!("../../tests/demo_structs/role.yml")).unwrap();
    /// assert_eq!("Not She/Her/Hers", role.name);
    ///
    /// let pronoun_role = PronounRole::new(role, Some("Not"));
    /// assert_eq!("She/Her/Hers", pronoun_role.base());
    /// ```
    pub fn base(&self) -> &str {
        &self.role.name[self.base.clone()]
    }

    /// Returns a reference to the role's modifier, if it exists.  If the role
    /// does not have a modifier (such as a role name "E/Im/Is", this will
    /// return `None`.
    ///
    /// # Example
    /// ```
    /// # use serde_yaml::from_str;
    /// # use serenity::model::guild::Role;
    /// # use libpronouner::pronoun::PronounRole;
    /// // Assuming role has already been defined.
    /// # let role: Role
    /// # = from_str(include_str!("../../tests/demo_structs/role.yml")).unwrap();
    /// assert_eq!("Not She/Her/Hers", role.name);
    ///
    /// let pronoun_role = PronounRole::new(role, Some("Not"));
    /// assert_eq!(Some("Not"), pronoun_role.modifier());
    /// ```
    pub fn modifier(&self) -> Option<&str> {
        self.modifier.clone().map(|m| &self.role.name[m])
    }

    /// A modified version of [`modifier()`](PronounRole::modifier) that returns an owned
    /// [`String`].
    ///
    /// This is provided for easy conformity with the [`Modifier`] type
    /// definition, which is accepted by many other methods.
    pub fn modifier_owned(&self) -> Modifier {
        self.modifier().map(str::to_string)
    }

    /// Creates a new PronounRole by wrapping a [`Role`].
    ///
    /// This only takes a modifier instead of a modifier and a base because the
    /// base is infered from the modifier.  To indicate that this role has no
    /// associated modifier, pass in None.  It is expected that if a modifier is
    /// included, then exactly one space exists between the modifier and the
    /// base of the pronoun, and that no extra characters exist.  If no modifier
    /// is included, then the entire name of the role is used as the base.
    pub fn new(role: Role, modifier: Option<impl AsRef<str>>) -> Self {
        let m = modifier.map(|m| 0..m.as_ref().len());
        Self {
            base: m.clone().map_or(0, |m| m.end + 1)..role.name.len(),
            role,
            modifier: m,
        }
    }

    /// Creates a new pronoun role by infering the modifier and base.
    ///
    /// This works under the assumption that the base will be
    /// formatted as a three-form pronoun with capitals (e.g. Ne/Nem/Nirs not
    /// Ne/Nem or ne/nem/nirs).  Any pronouns with a special form should be
    /// handled by [`parse_s()`][PronounRole::parse_s].
    pub fn parse<R: Borrow<Role>> (role: R) -> Result<Self, &'static str> {
        Self::parse_s(role, Vec::<String>::new())
    }

    /// Creates a pronoun role by infering modifier and base, including specials
    ///
    /// This will fail iff the pronoun base fails to conform to a
    /// slash-deliminad three-form format and is not listed in the provided specials.  This is case
    /// sensative, and each form is expected to start with a capital letter,
    /// followed by only lowercase letters and apostrophes until either a slash
    /// or the end of the role.  If a modifier is included, it must begin the
    /// role be separated from the base by a single space.
    ///
    /// # Examples
    /// The following examples assume that the special pronouns "Spivak" and "Any
    /// Pronouns" have been passed in.
    ///
    /// **Acceptable Pronoun Names**
    /// - E/Em/Eir
    /// - Any Pronouns
    /// - Trying Spivak
    /// - Y'all/Y'all/Y'all's
    ///
    /// **Unacceptable Pronoun Names**
    /// - e/em/eir
    /// - Ey/Em
    /// - E/Em/Es/Es/Emself
    /// - PreferSpivak
    /// - Prefer *(two spaces)* Any Pronouns
    /// - Em Em Ems
    /// - any pronouns
    /// - Unlisted Special
    ///
    ///```
    /// # use serde_yaml::from_str;
    /// # use serenity::model::guild::Role;
    /// # use libpronouner::pronoun::PronounRole;
    /// // Assuming role has already been defined.
    /// # let role: Role
    /// # = from_str(include_str!("../../tests/demo_structs/role_special.yml")).unwrap();
    /// assert_eq!("Trying No Pronouns", role.name);
    ///
    /// let pronoun_role = PronounRole::parse_s(role, vec!["No Pronouns"])
    ///     .unwrap();
    /// assert_eq!("No Pronouns", pronoun_role.base());
    /// assert_eq!(Some("Trying"), pronoun_role.modifier());
    pub fn parse_s<R, I, S>(role: R, specials: I)
    -> Result<Self, &'static str>
    where R: Borrow<Role>, I: IntoIterator<Item=S>, S: AsRef<str> {
        let role = role.borrow();
        if let Some(cap) = PRONOUN_REGEX.captures(&role.name) {
            let base = cap.name("base").map(|m| m.start()..m.end()).unwrap();
            let modifier = cap.name("modifier").map(|m| m.start()..m.end());
            Ok(Self {
                role: role.to_owned(),
                base,
                modifier,
            })
        } else {
            let matching_special = specials.into_iter()
                .find(|s| role.name.ends_with(s.as_ref()));
            if let Some(special) = matching_special {
                let modifier = Some(
                        role.name[..role.name.len() - special.as_ref().len()]
                            .trim()
                    )
                    .filter(|m| !m.is_empty());
                Ok(Self::new(role.to_owned(), modifier))
            } else {
                Err("Failed to parse from provided value:  No matches found.")
            }
        }
    }
}

impl std::hash::Hash for PronounRole {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.role.id.hash(state);
    }
}

impl PartialEq<PronounRole> for PronounRole {
    /// Computes the equality of the roles of this pronoun
    ///
    /// This **does not** examine how the base and modifier are split, and
    /// assumes that if a role can be parsed to a pronoun role, there is only
    /// one way to do it.  This is true if you do not use [`PronounRole::new()`]
    /// and use a consistant set of special pronouns.
    fn eq(&self, other: &Self) -> bool {
        self.role.id.eq(&other.role.id)
    }
}

impl core::convert::TryFrom<Role> for PronounRole {
    type Error = &'static str;
    fn try_from(role: Role) -> Result<Self, Self::Error>{
        Self::parse(role)
    }
}

impl fmt::Display for PronounRole {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.role.name.fmt(f)
    }
}

impl From<PronounRole> for RoleId {
    fn from(prole: PronounRole) -> Self {
        prole.role.into()
    }
}

/// A collection of [`PronounRole`]s with a common base pronoun.
///
/// This struct, thinly wrapping a [`HashMap`] mapping modifiers to roles,
/// represents a collection of similar roles.  For each variant on the base
/// role, one entry exists in the underlying hashmap.  That entry contains the
/// [`Modifier`] of that role, as well as the [`Role`] itself.  Using these two
/// pieces of information with the base used in this pronoun set, we can
/// compiler a complete [`PronounRole`], as several methods in this struct do.
#[derive(Clone, Debug, Eq, PartialEq)]
pub struct PronounSet {
    modifiers: HashMap<Modifier, Role>,
    base: String,
}

impl PronounSet {
    /// Yeilds the base pronouns common to this set.
    ///
    /// For example, for a [`PronounSet`] containing "Not Ze/Zem/Zes", "Trying
    /// Ze/Zem/Zes", and "Ze/Zem/Zes", this would yield "Ze/Zem/Zes"
    pub const fn base(&self) -> &String {
        &self.base
    }

    /// Adds a pronoun role to this set
    ///
    /// This should not be called with a role that has a different base than the
    /// `PronounSet`'s [`base`](PronounSet::base).  The role will be listed
    /// under the modifier listed in [`PronounRole::modifier`].  This also means
    /// that if the remainder of the role will be used as the base if this role
    /// is retreived via [`get_pr()`](PronounSet::get_pr).
    pub fn add_pr(&mut self, role: PronounRole) {
        debug_assert_eq!(role.base(), self.base(),
            "Mismatching base on PronounRole being added to PronounSet"
        );
        self.modifiers.insert(role.modifier_owned(), role.role);
    }

    /// Retrieves a role from the set as a [`PronounRole`]
    ///
    /// Set the modifier to [`None`] to retrieve the base role, if present.
    /// Returns [`None`] when the requested role is not present.
    pub fn get_pr(&self, modifier: &Modifier) -> Option<PronounRole> {
        self.get(modifier)
            .cloned()
            .map(|r| PronounRole::new(r, modifier.as_ref()))
    }

    /// Creates a new `PronounSet` with the provided base
    pub fn new(base: String) -> Self {
        Self {
            modifiers: HashMap::new(),
            base,
        }
    }

    /// Attempts to get the requested role, and creates one if it isn't present.
    ///
    /// **IMPORTANT:** This methods does **not** add the created role to the set
    /// after creating it.  This is to enable this method to be used without a
    /// mutable reference, as often may be the case.  Many users may find it
    /// easier rebuild their [`PronounDatabase`] each time a command is run, as
    /// this approach relieves the need to constantly watch and record role
    /// changes, while rebuilding the database does not occur very often.  If
    /// you do want to keep a running database, it may serve you better to not
    /// use a [`PronounDatabase`] or `PronounSet`s, which are not designed to be
    /// presistant.
    ///
    /// The second, `create_with` argument should be an FnOnce taking two
    /// arguments.  The first is a String, representing the name of the role
    /// that should be created.  The second is an optional reference to another
    /// role, which may be used as a model, for example when inheriting
    /// properties.  The model role is guarenteed to be another role from this
    /// same set, and will only be `None` if there are no other pronouns in this
    /// set.  The `create_with` function should return a result with the newly
    /// created role.  If `create_with` returns an `Err`, then that error is
    /// returned by the `get_or_create`.
    pub async fn get_or_create<E, FR>(
        &self,
        modifier: Modifier,
        create_with: impl FnOnce(String, Option<&Role>) -> FR
    ) -> Result<PronounRole, E>
    where
        FR: Future<Output = Result<Role, E>>
    {
        self._get_or_create(modifier, create_with, || None).await
    }

    /// A helper method for [`PronounSet::get_or_create`]
    ///
    /// This version adds an `alternate_model` argument, which is called if
    /// there are no [`PronounRole`]s existing within this PronounSet.  If this
    /// is the case, the result from this method is used as the argument to the
    /// `create_with` method instead.  This is really just used for calling
    /// `get_or_create` from a [`PronounDatabase`].
    async fn _get_or_create<'a, E, FR>(
        &'a self,
        modifier: Modifier,
        create_with: impl FnOnce(String, Option<&Role>) -> FR,
        alternate_model: impl FnOnce() -> Option<&'a Role>,
    ) -> Result<PronounRole, E>
    where
        FR: Future<Output = Result<Role, E>>
    {
        let existing_role = self.get(&modifier)
            .cloned();

        let final_role = if let Some(role) = existing_role {
            role
        } else {
            let model_role = self.values()
                .next()
                .or_else(alternate_model);

            (create_with)(
                role_name_from_components(&self.base, &modifier),
                model_role,
            ).await?
        };

        Ok(PronounRole::new(
            final_role,
            modifier,
        ))
    }

    /// Returns an iterable over all of the [`PronounRole`]s in this
    /// `PronounSet`.  This only returns [`PronounRole`]s that have already been
    /// created and added to the set, and could be returned by
    /// [`get_pr()`](PronounSet::get_pr).  While calling this method does not
    /// cause any clones to occur, each call to [`Iterator::next`] on the
    /// resulting iterator will clone a [`Role`].
    ///
    /// ```rust,no_run
    /// # use libpronouner::pronoun::*;
    /// # let pronoun_set: PronounSet = None.unwrap();
    /// // Assuming a `pronoun_set` has been created
    /// for pronoun_role in pronoun_set.iter_pr() {
    ///     println!(
    ///         "Role #{} is {}",
    ///         pronoun_role.role.id,
    ///         role_name_from_components(
    ///             pronoun_role.base(),
    ///             &pronoun_role.modifier()
    ///         ),
    ///     );
    /// }
    /// ```
    pub fn iter_pr(&self) -> impl Iterator<Item=PronounRole> + '_ {
        self.modifiers
            .iter()
            .map(|p| PronounRole::new(p.1.clone(), p.0.clone()))
    }
}

impl Deref for PronounSet {
    type Target = HashMap<Modifier, Role>;

    fn deref(&self) -> &Self::Target {
        &self.modifiers
    }
}

/// A collection of various [`PronounSet`]s organized by base.
///
/// Typically, one `PronounDatabase` represents the full set of roles on one
/// guild.  It's worth noting that as with the [`PronounSet`], `PronounDatabase`
/// is not designed to be persistent.  After being populated and used, it is
/// recommended to completely discard it.  The preferred way of constructing a
/// `PronounDatabase` is using the [`with_roles()`](PronounDatabase::with_roles)
/// method, which allows easy conversion from a guild.
#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct PronounDatabase(HashMap<String, PronounSet>);

impl PronounDatabase {
    /// Creates an empty `PronounDatabase`.
    ///
    /// If you construct a `PronounDatabase` this way, you will be responsible
    /// for populating it yourself, using [`add_pr()`](PronounDatabase::add_pr)
    /// and [`add_roleless_pronoun()`](PronounDatabase::add_pr).  It's
    /// recomended to instead use the
    /// [`with_roles()`](PronounDatabase::with_roles) method, or call
    /// [`Iterator::collect`] on an iterable of either [`Role`]s or
    /// [`PronounRole`]s.
    pub fn new() -> Self {
        Self(HashMap::new())
    }

    /// Given the beginning of a pronoun, attempt to find the full form.
    ///
    /// This can be useful for finding a [`PronounSet`] when you only have a
    /// short form pronoun like "they/them" or even just "they".  This also is
    /// case insensitive.  This can be very useful for finding a pronoun based
    /// on user input without constraining your users to a restrictive set of
    /// rules on how to input their pronouns.  This is an O(n) operation if
    /// there isn't an exact match, but I'd like to see your pronoun database
    /// that's so big that this is a problem.
    ///
    /// The output of this method is a reference to a [`PronounSet`], which
    /// allows you to either extract the matching base using
    /// [`PronounSet::base`], or find a specific [`PronounRole`] if you know the
    /// modifier of that role.
    ///
    /// This method does not handle modifiers.  Please parse those out yourself.
    /// However, it will handle special pronouns (e.g. Use Name, or Any
    /// Pronouns).
    ///
    /// In the event that there are multiple pronoun sets that match the
    /// provided stub, there is no guarantee which one will be returned.  And if
    /// no pronoun set is found, you may be surprised to learn that `None` is
    /// returned.  (that was sarcasm.  I'm bored).  If there are no pronouns in
    /// the matching [`PronounSet`] (i.e. it was added using
    /// [`PronounDatabase::add_roleless_pronoun`], it is still returned.
    ///
    /// **Note:** The only difference between this method and calling
    /// `next()` on [`find_pronouns_by_stub()`] is the lifetime of the returned type.
    ///
    /// # Example
    /// ```
    /// # use libpronouner::pronoun::PronounDatabase;
    /// let mut pronoun_db = PronounDatabase::new();
    ///
    /// // Normally you'd use role based pronouns here, but this works fine.
    /// pronoun_db.add_roleless_pronouns(vec![
    ///     "Xe/Xem/Xers",
    ///     "Thon/Thon/Thons",
    ///     "Any Pronouns",
    /// ]);
    ///
    /// assert_eq!(
    ///     pronoun_db.find_pronoun_by_stub("xe").unwrap().base(),
    ///     "Xe/Xem/Xers",
    /// );
    ///
    /// // Also works with specials!
    /// assert_eq!(
    ///     pronoun_db.find_pronoun_by_stub("any").unwrap().base(),
    ///     "Any Pronouns",
    /// );
    /// ```
    pub fn find_pronoun_by_stub(&self, stub: impl AsRef<str>) -> Option<&PronounSet> {
        if self.contains_key(stub.as_ref()) {
            self.get(stub.as_ref())
        } else {
            self.find_pronouns_by_stub(stub).next()
        }
    }

    /// Given the beginning of a pronoun, attempt to find the full form.
    ///
    /// This method is effectively the same as [`find_pronoun_by_stub`], but will return
    /// an iterator over multiple possible values, in an arbitrary order.
    ///
    /// Advantages of this method include the ability to filter and prioritize different
    /// pronoun sets, such as matching only pronouns that a user already uses, or choosing
    /// the most popular of all of the possibilities.
    ///
    /// # Example
    /// ```
    /// # use libpronouner::pronoun::PronounDatabase;
    /// let mut pronoun_db = PronounDatabase::new();
    ///
    /// // Normally you'd use role based pronouns here, but this works fine.
    /// pronoun_db.add_roleless_pronouns(vec![
    ///     "He/Her/Hers",
    ///     "He/Him/His",
    ///     "They/Them/Theirs",
    /// ]);
    ///
    /// // Make a list of all of the pronoun sets (as strings) that match a search for he
    /// let results: Vec<&str> = pronoun_db.find_pronouns_by_stub("he")
    ///                                            .map(|p| p.base().as_ref())
    ///                                            .collect();
    ///
    /// assert!(
    ///     results.contains(&"He/Him/His") &&
    ///     results.contains(&"He/Her/Hers")
    /// );
    ///
    /// assert!(
    ///     ! results.contains(&"They/Them/Theirs")
    /// );
    /// ```
    pub fn find_pronouns_by_stub(
        &self,
        stub: impl AsRef<str>,
    ) -> impl Iterator<Item = &PronounSet> {
        self.iter()
            .filter(move|p|
                p.0.len() >= stub.as_ref().len() &&
                p.0[..stub.as_ref().len()].eq_ignore_ascii_case(stub.as_ref()))
            .map(|p| p.1)
    }

    /// Adds a [`PronounRole`] to this database.
    ///
    /// This handles sorting it under the correct base and modifier.  However,
    /// the [`PronounRole`] itself is not stored, only the information required
    /// to reconstruct it.  This does however keep the [`Role`].
    pub fn add_pr(&mut self, role: PronounRole) {
        self.add_roleless_pronoun(
            role.base().to_string()
        ).add_pr(role);
    }

    /// Retrieves a [`PronounRole`] from the database.
    ///
    /// This will wrap the [`Role`] in a [`PronounRole`].  If you want a little
    /// more control, you can dereference a [`PronounDatabase`] into a
    /// [`HashMap`]<String, [`PronounSet`]>, and a [`PronounSet`] into a
    /// [`HashMap`]<[`Modifier`], [`Role`]>.  This method is just for
    /// convenience.
    pub fn get_pr(&self, base: &str, modifier: Modifier) -> Option<PronounRole> {
        self.get(base)
            .and_then(|base| base.get_pr(&modifier))
    }

    /// Attempt to find a matching [`PronounRole`], or else create one.
    ///
    /// This is very similar to [`PronounSet::get_or_create`] in it's behavior.
    /// Close enough that I'm too lazy to rewrite all those docs.  However the
    /// one difference is that this the model role provided in this method may
    /// come from any [`PronounRole`] in the entire database, not just the
    /// corresponding [`PronounSet`].  However, roles that are in the same
    /// pronoun set are given higher priority, meaning that a [`Role`] from
    /// outside of the [`PronounSet`] will only be given if there are now roles
    /// in the existing [`PronounSet`].
    pub async fn get_or_create<FR, E>(
        &self,
        base: impl AsRef<str>,
        modifier: Modifier,
        create_with: impl FnOnce(String, Option<&Role>) -> FR
    ) -> Result<PronounRole, E>
    where
        FR: Future<Output = Result<Role, E>>
    {
        let temp;
        let pronoun_set = if let Some(base) = self.get(base.as_ref()) {
            base
        } else {
            temp = PronounSet::new(base.as_ref().to_string());
            &temp
        };

        pronoun_set._get_or_create(
            modifier,
            create_with,
            || {
                self.values()
                    .flat_map(|m| m.values())
                    .next()
            },
        ).await
    }

    /// Iterate over all [`PronounRole`]s in this database
    ///
    /// This produces a [`Clone`]-able [`Iterator`] over all [`PronounRole`]s in
    /// this database.  This will not include any of the pronouns added by
    /// [`PronounDatabase::add_roleless_pronoun`] unless an actual role has been
    /// added with that base.  As such, please don't use this to generate a list
    /// of available pronouns.  While calling this method doesn't cause any
    /// clones to occur, each call to [`Iterator::next`] on the resulting
    /// iterator will require cloning a [`Role`], as with the sister method
    /// [`PronounSet::iter_pr`].
    pub fn list_roles(&self)
    -> impl Iterator<Item=PronounRole> + '_ {
        self.values()
            .flat_map(|map| map.iter_pr())
    }

    /// Adds an empty [`PronounSet`] with the given base
    ///
    /// This adds a base to the database without needing to attach any
    /// associated roles.  This is particularly useful when you want to be able
    /// to add a pronoun to the database when it might not otherwise be able to
    /// be added, mostly with special pronouns.  A mutable reference to the
    /// resulting [`PronounSet`] is returned.  Note that because this doesn't
    /// add any roles to the database, it will not show up in
    /// [`PronounDatabase::list_roles`] or [`PronounDatabase::get_pr`], but will
    /// show up in [`find_pronoun_by_stub`](PronounDatabase::find_pronoun_by_stub)
    pub fn add_roleless_pronoun(&mut self, base: String) -> &mut PronounSet {
        self.0.entry(base.clone())
            .or_insert_with(|| PronounSet::new(base))
    }

    /// An alternate version of
    /// [`add_roleless_pronoun`](PronounDatabase::add_roleless_pronoun) that
    /// takes an [`Iterator`]
    ///
    /// Equivalent to
    /// ```
    /// # use libpronouner::pronoun::PronounDatabase;
    /// # let mut pronoun_db = PronounDatabase::new();
    /// # let mut pronoun_db2 = PronounDatabase::new();
    /// # let pronouns = vec!["She/Her/Hers", "Any Pronouns", "No Pronouns"];
    /// for pronoun in &pronouns {
    ///     pronoun_db.add_roleless_pronoun(pronoun.to_string());
    /// }
    /// # pronoun_db2.add_roleless_pronouns(pronouns);
    /// # assert_eq!(pronoun_db, pronoun_db2);
    /// ```
    pub fn add_roleless_pronouns<S, I>(&mut self, pronouns: I)
    where S: ToString, I: IntoIterator<Item=S> {
        for pn in pronouns {
            self.add_roleless_pronoun(pn.to_string());
        }
    }

    /// Produces a [`PronounDatabase`] from a list of roles and special roles.
    ///
    /// This is the preferred way to generate a [`PronounDatabase`], as
    /// alternative methods require rewriting the wheel or neglecting special
    /// pronouns in the entry set.  However, if you don't have any special
    /// pronouns, you are welcome to use the [`FromIterator`]<[`PronounRole`]>
    /// method, which ends up being sugar syntax for this method.
    ///
    /// A [`PronounSet`] is guaranteed to be included for all strings in
    /// `specials`, even if no matching pronouns are included in the `roles`.
    /// In this case, the [`PronounSet`] will be empty.
    ///
    /// Roles in the `roles` argument are only included if they can be parsed
    /// into a [`PronounRole`].  At the moment, the criteria for this are the
    /// same as [`PronounRole::parse_s`]
    ///
    /// If you already have a list of [`PronounRole`]s (note, not [`Role`]s),
    /// then it's best to use the second [`FromIterator`]<[`PronounRole`]> method
    /// here instead.
    pub fn with_roles<IR, IS, R, S, C>(roles: IR, specials: IS)
    -> Self
    where IR: IntoIterator<Item = R>,
    IS: IntoIterator<Item = S, IntoIter = C>, R: Borrow<Role>,
    S: AsRef<str>, C: Iterator<Item = S> + Clone {
        let specials = specials.into_iter();

        let mut db: Self = roles.into_iter()
            .filter_map(|r| PronounRole::parse_s(r, specials.clone()).ok())
            .collect();

        db.add_roleless_pronouns(specials.map(|s| s.as_ref().to_string()));
        db
    }

    /// Calls [`PronounStats::calculate`] using this database's roles
    ///
    /// This method assumes that all of the roles that have been added to this
    /// database are from the same guild.  Also, can return a
    /// [`ModelError::GuildNotFound`](serenity::model::ModelError::GuildNotFound)
    /// if there is a problem finding the appropriate guild.
    pub async fn calc_stats(
        &self,
        cache: impl AsRef<serenity::cache::Cache>
    ) -> serenity::Result<PronounStats> {
        if let Some(PronounRole{role, ..}) = self.list_roles().next() {
            if let Some(guild) = role
                    .guild_id
                    .to_guild_cached(cache)
                    .await
            {
                Ok(PronounStats::calculate(&guild, self))
            } else {
                Err(
                    serenity::Error::Model(
                        serenity::model::ModelError::GuildNotFound
                    )
                )
            }
        } else {
            Ok(PronounStats::default())
        }
    }
}

impl<R> FromIterator<R> for PronounDatabase
where R: Borrow<Role> {
    fn from_iter<I: IntoIterator<Item = R>>(roles: I) -> Self {
        Self::from_iter(
            roles.into_iter()
                .filter_map(|r| PronounRole::parse(r).ok())
        )
    }
}

impl FromIterator<PronounRole> for PronounDatabase {
    fn from_iter<I: IntoIterator<Item=PronounRole>>(roles: I) -> Self {
        let mut db = Self::new();

        for role in roles {
            db.add_pr(role);
        }

        db
    }
}

impl Deref for PronounDatabase {
    type Target = HashMap<String, PronounSet>;

    fn deref(&self) -> &HashMap<String, PronounSet> {
        &self.0
    }
}

/// Convert a base and a modifier to a string
///
/// It can be annoying sometimes if you have a base and a [`Modifier`] that you
/// want to print as a string.  You need a pattern match and more lines that
/// really ought to be necessary.  This function exists to simplify that
/// process.  It effectively just prepends the modifier to the base with a space
/// if the modifier exists, and then returns the result.
///
/// # Example
/// ```
/// # use libpronouner::pronoun::*;
/// let base = "Shi/Hir/Hirs";
/// let mut modifier = Some("Trying");
///
/// assert_eq!(
///     role_name_from_components(base, &modifier),
///     "Trying Shi/Hir/Hirs"
/// );
///
/// modifier = None;
///
/// assert_eq!(
///     role_name_from_components(base, &modifier),
///     "Shi/Hir/Hirs"
/// );
/// ```
pub fn role_name_from_components(
    base: &str,
    modifier: &Option<impl AsRef<str>>
) -> String {
    if let Some(modif) = modifier.as_ref() {
        format!("{} {}", modif.as_ref(), base)
    } else {
        base.to_string()
    }
}

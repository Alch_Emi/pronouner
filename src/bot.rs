// Copyright 2019 Emi Simpson <emi@alchemi.dev>

// This file is part of the Pronouner project.
//
// The Pronouner project is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// The Pronouner project is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
// for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with the Pronouner project.  If not, see <https://www.gnu.org/licenses/>.

extern crate regex; extern crate serenity;

use async_trait::async_trait;

use lazy_static::lazy_static;

use log::{debug, info, warn, error};

use regex::Regex;

use serenity::{
    prelude::*,
    client::EventHandler,
    Error::*,
    http::Http,
    model::{
        prelude::*,
        channel::{
            Channel::*,
            Message,
            PrivateChannel,
        },
        gateway::Ready,
        guild::{
            Member,
            Guild,
        },
        user::User,
    },
    utils::Colour,
};

use itertools::Itertools;

use futures::{
    stream::FuturesUnordered,
    TryStreamExt,
    join,
};

use std::{
    collections::{
        HashSet,
        HashMap,
    },
    cmp::Ordering,
    convert::TryInto,
    fmt::Display,
    future::Future,
    hash::BuildHasher,
    sync::Arc,
};

use crate::pronoun::*;

#[cfg(feature = "pluralkit")]
use crate::pluralkit::*;

lazy_static! {
    static ref PREFIX_REGEX: Regex = Regex::new(
        r"^!p(?i)(?:n|ronoun)(?:s?)\b"
    ).unwrap();
}
const REQUIRED_PERMS: Permissions = Permissions { bits:
    Permissions::READ_MESSAGES.bits() |
    Permissions::SEND_MESSAGES.bits() |
    Permissions::MANAGE_ROLES.bits() |
    Permissions::EMBED_LINKS.bits()
};

/// A struct facilitating the Discord connectivity for the bot
///
/// This struct contains a list of modifiers and special pronouns, and offers
/// utility methods for tasks that might be common for a bot interacting with
/// pronouns on a Discord server.
///
/// Because this struct also implements both [`serenity::framework::Framework`]
/// and [`serenity::client::EventHandler`], it can directly be used when
/// creating a bot as the bot's event handler, and cloned to also be used as a
/// framework.  It's recommend that you do neither or both of these things, but
/// not only one.  Using this struct as a framework enables most commands and
/// bot functionality, but will not automatically chunk guilds, meaning that not
/// all members will be accounted for, and some people might lose their roles
/// either during a transition or when someone removes a role.
///
/// It is also possible to not register this struct at all, and only use it's
/// utility methods by passing in a [`Context`] and several arguments.  If you
/// do not want a specific functionality of the bot and are really just planning
/// on making a complementary service, this is probably best for you.
#[derive(Clone)]
pub struct PronounBot {
    /// A list of all modifiers used by this bot
    modifiers: Vec<Arc<String>>,

    /// Any pronouns that need to be persisted regardless of if there is a role for them.
    ///
    /// This often includes special pronouns like "Any Pronouns".  These are added to the
    /// [`PronounDatabase`] by way of [`PronounDatabase::add_roleless_pronouns`].  The
    /// reason they need to be tracked here is because the database is constantly being
    /// re-created, and these pronouns may be lost if it is.
    specials: Vec<String>,

    /// A table to lookup a modifier by it's synonym.
    ///
    /// All synonyms are stored as lowercase [`String`]s as the keys, pointing
    /// to a reference counted pointer of the appropriate modifier (normal
    /// case).  This will be the same reference as the instance in the modifiers
    /// feild.
    mod_synonyms: HashMap<String, Arc<String>>
}

impl PronounBot {
    /// Creates a new [`PronounBot`].
    ///
    /// The modifiers should be provided in a hashmap keying the modifier's
    /// proper form (in the appropriate case) to all possible synonyms for that
    /// modifier (case agnostic).
    ///
    /// The provided modifiers and specials will be stored indefinitely, and
    /// used for all functionality of  the bot.
    pub fn new<S: AsRef<str>, H: BuildHasher>(
        modifiers: HashMap<impl ToString, impl IntoIterator<Item=S>, H>,
        specials: Vec<String>
    ) -> Self {
        let mut modifiers_mapped: _ = Vec::with_capacity(modifiers.len());
        let mut mod_synonyms: _ = HashMap::with_capacity(modifiers.len());

        for modipair in modifiers {

            let proper_form = Arc::new(modipair.0.to_string());

            let synonyms = modipair.1.into_iter()
                .map(|s| s.as_ref().to_lowercase())
                .chain(Some(proper_form.to_lowercase()));

            for synonym in synonyms {
                mod_synonyms.insert(
                    synonym.to_lowercase(),
                    Arc::clone(&proper_form),
                );
            }

            modifiers_mapped.push(proper_form);
        }

        Self {
            modifiers: modifiers_mapped,
            specials,
            mod_synonyms,
        }
    }

    /// Given a certain [`Message`], find the sender of that message as a
    /// [`Member`].
    ///
    /// This should really only be used when the provided message was received
    /// directly in an event, to avoid a panic.
    ///
    /// # Panics
    /// Panics if the message was not sent in a guild, or the guild it was sent
    /// in could not be found in the cache.
    async fn get_member(ctx: &Context, msg: &Message) -> serenity::Result<Member> {
        msg.guild(ctx)
            .await
            .unwrap()
            .member(ctx, msg.author.id)
            .await
    }

    /// Identify the corresponding [`PronounSet`] and [`Modifier`] based on user input.
    ///
    /// This takes a list of arguments and attempts to match them with a
    /// corresponding [`PronounSet`] and [`Modifier`].  The return value of this
    /// method must always return a [`PronounSet`] that is a reference to a set
    /// stored in a [`PronounDatabase`] and a [`Modifier`] that can be found in
    /// the collection of modifiers that the `PronounBot` was constructed with.
    ///
    /// This will handle cases where the input contains several forms of a
    /// pronoun that aren't connected with slashes, for example if the input is
    /// `["trying", "it", "it"]`, this could be parsed to `("It/It/Its",
    /// Some("Trying"))`.  As you can see, this also handles case sensitivity
    /// and missing forms, as with [`PronounDatabase::find_pronoun_by_stub`].
    ///
    /// In addition, modifier synonyms are supported here, meaning that if the
    /// bot was constructed listing "not" as a synonym for "Except", (and
    /// Kit/Kit/Kits is listed as a pronoun) then both "not kit" and "except
    /// kit" will yeild ("Kit/Kit/Kits", "Except")
    ///
    /// Also note that this method cannot partially succeed.  If no matching
    /// modifier (or lack thereof) can be found, no [`PronounSet`] will be
    /// returned.  If a modifier matches, but not a [`PronounSet`], then neither
    /// modifier nor [`PronounSet`] is returned.
    ///
    /// Additionally, while this method can handle a split base (e.g. the
    /// example above), it cannot handle a split or incomplete modifier.  This
    /// is why it's recommended to limit modifiers to one word.
    ///
    /// # Example
    /// ```
    /// # use libpronouner::pronoun::PronounDatabase;
    /// # use libpronouner::PronounBot;
    /// # use std::collections::HashMap;
    /// // Assuming `pronoundb` and `bot` have already been initialized with the
    /// // correct modifiers and special pronouns, and "maybe" is a synonym for
    /// // "Trying"
    /// # let mut pronoundb = PronounDatabase::new();
    /// # pronoundb.add_roleless_pronouns(vec![
    /// #     "It/It/Its",
    /// #     "No Pronouns",
    /// # ]);
    /// # let syns: HashMap<String, Vec<&str>> =
    /// #     [
    /// #         ("Trying".to_string(), vec!["maybe"]),
    /// #         ("Except".to_string(), vec!["no"]),
    /// #         ("In Person,".to_string(), vec!["in person"]),
    /// #     ].iter().cloned().collect();
    /// # let bot = PronounBot::new(
    /// #     syns,
    /// #     vec!["No Pronouns".to_string()],
    /// # );
    ///
    /// // Example from above, as code
    /// let (set, modifier) = bot.parse_args(
    ///     &pronoundb,
    ///     &vec!["trying", "it", "it"]
    /// ).next().unwrap();
    ///
    /// assert_eq!(set.base(), "It/It/Its");
    /// assert_eq!(modifier, Some("Trying".to_string()));
    ///
    /// // Also works with specials!
    /// let (set, modifier) = bot.parse_args(
    ///     &pronoundb,
    ///     &vec!["mAybe", "no"]
    /// ).next().unwrap();
    ///
    /// assert_eq!(set.base(), "No Pronouns");
    /// assert_eq!(modifier, Some("Trying".to_string()));
    ///
    /// # // Bug found 2020-07-01: Fails to parse "no pronouns" without modifiers if "no"
    /// # // exists as a modifier
    /// # let (set, modifier) = bot.parse_args(
    /// #     &pronoundb,
    /// #     &vec!["no", "pronouns"]
    /// # ).next().unwrap();
    /// #
    /// # assert_eq!(set.base(), "No Pronouns");
    /// # assert_eq!(modifier, None);
    /// #
    /// # // Bug found 2020-03-23: Fails to identify multi-word modifiers
    /// # let (set, modifier) = bot.parse_args(
    /// #     &pronoundb,
    /// #     &vec!["in", "person", "it"]
    /// # ).next().unwrap();
    /// #
    /// # assert_eq!(set.base(), "It/It/Its");
    /// # assert_eq!(modifier, Some("In Person,".to_string()));
    /// ```
    #[allow(clippy::useless_let_if_seq)]
    pub fn parse_args<'a>(
        &self,
        pronouns: &'a PronounDatabase,
        args: &[&str],
    ) -> impl Iterator<Item=(&'a PronounSet, Modifier)> {

        // Maintain a list of possible interpretations of the arguments as a tuple
        // including the modifier (optionally), and the base.  There's likely to be
        // several of these, since special pronouns need to be counted to.
        //
        // Example:
        //
        // ["no", "pro"] could be:
        //   - ("Except", "pro") because no is a synonym for except
        //   - (None, "no/pro") where the args are parts of a pronoun set (join with "/")
        //   - (None, "no pro") where the args are part of a special set (join with " ")
        let mut possible_combinations: Vec<(Option<String>, String)>;

        if args.len() == 1 {
            // If there is only one arg, then there's no chance it's a modifier, and we
            // don't need to worry about a missing space in a special pronoun
            possible_combinations = vec![(None, args[0].to_string())];
        } else {

            possible_combinations = Vec::with_capacity(4);

            let full_args_with_spaces = args.iter().join(" ").to_lowercase();

            // This is an iterator of all the possible modifiers that could be prefixed to
            // the given args, including no modifiers.  Each entry is a tuple containing
            // the modifier and the number of words it takes up.iterator of all the
            // possible modifiers that could be prefixed to the given args, including no
            // modifiers.  Each entry is a tuple containing the modifier and the number of
            // words it takes up.
            let modifier_cases = self.mod_synonyms.iter()
                .filter(|(syn, _)| full_args_with_spaces.starts_with(syn.as_str()))
                .map(|(syn, modif)| (
                    Some(modif.to_string()),
                    syn.split_ascii_whitespace().count(),
                ))
                .chain(std::iter::once((None, 0)));

            // For each of the possible cases above, try interpretting the remaining args
            // as either parts of a pronoun set joined by "/"s, or part of a special
            // joined by a space.
            for (modifier, modifier_len) in modifier_cases {

                let remaining_args = &args[modifier_len..];

                if remaining_args.len() == 1 {
                    possible_combinations.push((modifier, remaining_args[0].to_string()));
                } else {
                    possible_combinations.extend(vec![
                        (modifier.clone(), remaining_args.join(" ")),
                        (modifier, remaining_args.join("/")),
                    ]);
                }
            }
        }

        // For every combination of modifier and base, find all possible matches for the
        // base in the pronoun database.
        possible_combinations.into_iter()
            .flat_map(move|(modifier, base)| {
                pronouns.find_pronouns_by_stub(base)
                    .map(move|base| (base, modifier.clone()))
            })
    }

    /// Deletes roles only if they meet requirements for pruning
    ///
    /// These requirements effectively boil down to no one using this role and
    /// the ability to recreate this role if it's deleted.  For more information
    /// on the specific criteria used here, please see the docs of
    /// [`PronounStats::can_prune`].
    ///
    /// Any undeleted roles are returned, along with the number of deleted
    /// roles.  The `allow_one` argument is passed to
    /// [`can_prune`](`PronounStats::can_prune`).  This method assumes that all
    /// of the roles as well as the provided [`PronounDatabase`] are all from
    /// the same guild that is provided in the `guild` argument.
    pub async fn try_prune(
        &self,
        ctx: &Context,
        pronouns: &PronounDatabase,
        roles: impl IntoIterator<Item=PronounRole>,
        allow_one: bool,
    ) -> serenity::Result<(Vec<PronounRole>, usize)> {
        warn!("Pruning is disabled until a pending bug is fixed");
        return Ok((roles.into_iter().collect(), 0));

        let mut stats = pronouns.calc_stats(ctx).await?;
        let (mut prune, noprune) = stats.can_prune(roles, &self.specials, allow_one);

        let n_pruned = prune.len();

        prune
            .iter_mut()
            .map(|role| role.role.delete(ctx))
            .collect::<FuturesUnordered<_>>()
            .try_collect()
            .await?;

        Ok((noprune, n_pruned))
    }

    /// A function representation of the `!pronouns add` command
    ///
    /// (or just `!pronouns`).  This accepts a slice of parsed arguments, along
    /// with the Message that it was sent through and a [`PronounDatabase`] to
    /// reference existing pronouns from.  This will only add pronouns that
    /// exist in the [`PronounDatabase`], but may create a new [`Role`] under an
    /// existing pronoun base.
    ///
    /// The arguments that this accepts are parsed using
    /// [`PronounBot::parse_args`], and so share the same requirements as are
    /// described in the documentation of that method.
    ///
    /// A message is sent if pronouns are successfully added, or are not found
    /// within the give database.  Otherwise, a [`serenity::Error`] is returned
    /// detailing the problem.
    ///
    /// This runs a small and simple prune operation after adding the role
    ///
    /// This method is primarily publicised for use directly as a command in a
    /// Discord bot.  If you're looking to add pronouns not based off a user
    /// command, or don't want messages sent in response, perhaps try
    /// [`add_pronouns()`](PronounBot::add_pronouns).
    pub async fn add_pronouns_cmd(
        &self,
        ctx: &Context,
        args: &[&str],
        message: &Message,
        pronouns: &PronounDatabase,
    ) -> serenity::Result<()> {
        let mut member = match Self::generic_pk_checks(ctx, message).await? {
            None => return Ok(()),
            Some(member) => member,
        };


        if args.is_empty() {
            let sample_modifier = self.modifiers.get(0)
                .expect("At least one modifier must be used");
            message.channel_id.say(ctx,
                format!(
                    include_str!("../messages/cmds/add/help.txt"),
                    sample_modifier = sample_modifier,
                )
            ).await?;
        } else {
            let pronoun_stats = pronouns.calc_stats(ctx).await?;
            if let Some((set, modifier)) =
                self.parse_args(pronouns, args)
                    .max_by_key(|(set, _)|
                        pronoun_stats.user_counts_for_set(set)
                    )
            {
                Self::add_pronouns(
                    ctx,
                    pronouns,
                    &mut member,
                    set.base().to_string(),
                    modifier.clone(),
                ).await?;

                let pronouns = role_name_from_components(set.base(), &modifier);

                message.reply(ctx, format!(
                    include_str!("../messages/cmds/add/success.txt"),
                    pronouns=pronouns,
                )).await?;
            } else {
                message.reply(ctx, format!(
                    include_str!("../messages/cmds/add/missing.txt"),
                    pronouns=args.join("/"),
                )).await?;
            }
        }
        Ok(())
    }

    /// The method underlying [`add_pronouns_cmd`](PronounBot::add_pronouns_cmd)
    ///
    /// This method takes slightly different arguments than `add_pronouns_cmd`,
    /// and will never send messages in response.  This makes it more useful as
    /// a backend tool than the command variant.
    ///
    /// After adding the role, a quick check is run over the other roles in the
    /// same [`PronounSet`] to determine if any are now unnecessary and can be
    /// pruned.
    pub async fn add_pronouns(
        ctx: &Context,
        pronoundb: &PronounDatabase,
        member: &mut Member,
        base: String,
        modifier: Modifier,
    ) -> serenity::Result<()> {
        let role = pronoundb.get_or_create(
            &base,
            modifier,
            |name, model| {
                let model = model.cloned();
                let guild_id = member.guild_id;
                async move {
                    Self::create_role(guild_id, ctx, name, model.as_ref())
                        .await
                }
            }
        ).await?;

        let guild_id = role.role.guild_id;

        member.add_role(ctx, role.role.id).await?;

        pronoundb.calc_stats(ctx)
            .await?
            .user_counts(pronoundb)
            .iter()
            .filter(|(role, count)| **count == 0 && role.base() == base)
            .map(|(role, _count)| guild_id.delete_role(ctx, role.role.id))
            .collect::<FuturesUnordered<_>>()
            .try_collect()
            .await
    }

    /// A wrapper around [`remove_pronouns`](PronounBot::remove_pronouns) with
    /// argument parsing and responses.
    ///
    /// Used for the `!pronouns remove` command
    ///
    /// A response  will be sent on successfully removing pronouns, the user
    /// lacking the requested pronouns, or the pronouns not existing.  In all
    /// other cases, a [`serenity::Error`] will be returned.
    ///
    /// For more details regarding the behavior of this method, please see
    /// [`PronounBot::remove_pronouns`], the underlying method.
    pub async fn remove_pronouns_cmd(
        &self,
        ctx: &Context,
        args: &[&str],
        message: &Message,
        pronouns: &PronounDatabase,
    ) -> serenity::Result<()> {
        let mut member = match Self::generic_pk_checks(ctx, message).await? {
            None => return Ok(()),
            Some(member) => member,
        };

        if args.is_empty() {
            let sample_modifier = self.modifiers.get(0)
                .expect("At least one modifier must be used");
            message.channel_id.say(ctx,
                format!(
                    include_str!("../messages/cmds/remove/help.txt"),
                    sample_modifier = sample_modifier,
                )
            ).await?;
        } else if let Some(pronoun) =
            // Start with a list of possible argument interpretations
            self.parse_args(pronouns, args)
                // Only look at PronounRoles that the user already has (and change type)
                .filter_map(|(set, modifier)|
                    // Look up the pronoun role for the interpretation
                    set.get_pr(&modifier)
                        // Only consider it if the user has that role
                        .filter(|pr| member.roles.contains(&pr.role.id))
                        // If the user doesn't have the role, BUT the interpretation
                        // didn't include a modifier, it's possible that the user was
                        // trying to implicitly take off a modifier role, e.g. someone
                        // with the "Trying Peh/Pehm/Peh's" role might try to remove that
                        // with "!pronouns remove peh"
                        .or_else(||
                            if modifier.is_none() {
                                // So, go through the list of modified roles in this set,
                                // and look for one that the member has, returning that
                                // instead if we find one
                                set.iter_pr()
                                    .find(|pr| member.roles.contains(&pr.role.id))
                            } else { None }
                        )
                )
                .next()
        {
            let pronoun_str = pronoun.to_string();

            self.remove_pronouns(ctx, pronouns, &mut member, Some(pronoun)).await?;

            message.reply(ctx, format!(
                include_str!("../messages/cmds/remove/success.txt"),
                pronouns=pronoun_str,
            )).await?;
        } else {
            message.reply(ctx, format!(
                include_str!("../messages/cmds/remove/missing.txt"),
                pronouns=args.join("/"),
            )).await?;
        }

        Ok(())
    }

    /// A wrapper around [`clear_pronouns`](PronounBot::clear_pronouns) with
    /// argument parsing and responses.
    ///
    /// Used for the `!pronouns clear` command
    ///
    /// A response  will be sent if no [`serenity::Error`] occurs.  A successful
    /// message is sent even if the user has not [`PronounRole`]s.  There is no
    /// error message that can be sent.
    ///
    /// For more details regarding the behavior of this method, please see
    /// [`PronounBot::clear_pronouns`], the underlying method.
    pub async fn clear_pronouns_cmd(
        &self,
        ctx: &Context,
        message: &Message,
        pronouns: &PronounDatabase,
    ) -> serenity::Result<()> {
        let mut member = match Self::generic_pk_checks(ctx, message).await? {
            None => return Ok(()),
            Some(member) => member,
        };
        self.clear_pronouns(ctx, &mut member, pronouns).await?;
        message.reply(ctx, include_str!("../messages/cmds/clear.txt")).await?;
        Ok(())
    }

    /// Strip all of the pronoun roles from a given member
    ///
    /// This function examines the roles on a particular guild [`Member`], and
    /// identifies any roles that the provided [`PronounDatabase`] identifies as
    /// existing [`PronounRole`]s.  These roles are then efficiently removed and
    /// pruned, if necessary and applicable.
    pub async fn clear_pronouns(
        &self,
        ctx: &Context,
        member: &mut Member,
        pronouns: &PronounDatabase,
    ) -> serenity::Result<()> {
        let members_roles = member.roles.clone();

        let to_remove = pronouns.list_roles()
            .filter(|r| members_roles.contains(&r.role.id));

        self.remove_pronouns(ctx, pronouns, member, to_remove).await
    }

    /// Remove multiple [`PronounRole`]s from a member.
    ///
    /// This effectively just attempts to [`try_prune`] all provided roles, and
    /// then removes any unpruned roles from the member.
    ///
    /// [`try_prune`]: PronounBot::try_prune
    pub async fn remove_pronouns(
        &self,
        ctx: &Context,
        pdb: &PronounDatabase,
        member: &mut Member,
        pronouns: impl IntoIterator<Item=PronounRole>,
    ) -> serenity::Result<()> {

        // Attempt to prune eligable roles
        let leftovers = self.try_prune(ctx, pdb, pronouns, true).await?.0
            .into_iter()
            .map(|pr| pr.role.id)
            .collect_vec();

        // Remove any roles that didn't get deleted
        member.remove_roles(ctx, &leftovers).await?;

        Ok(())

    }

    /// DM's the user a [`handle_dm`]-readable message to start creating a new role.
    ///
    /// You might want to read the docs on [`handle_dm`] for an explanation of
    /// the exact mechanics by which this system handles direct messages.  This
    /// method also sends a reply in the original channel indicating that the
    /// user ought to check their DM's.
    ///
    /// [`handle_dm`]: PronounBot::handle_dm
    pub async fn start_create_cmd(
        ctx: &Context,
        message: &Message,
        guild_name: impl AsRef<str>,
    ) -> serenity::Result<()> {
        // Collect information about where this message came from
        let is_webhook = message.webhook_id.is_some();

        let user_id;
        #[cfg(feature = "pluralkit")] {
            // If the user is using PluralKit, look them up and give them a rundown on
            // what the bot can and can't do, then try to go ahead using the system's
            // account.
            let pk_info = Self::is_pk_message(message).await;
            user_id = pk_info.map(|pkmsg| pkmsg.sender);
        }
        #[cfg(not(feature = "pluralkit"))] {
            user_id = None;
        }

        let user_id = if let Some(pk_sender_id) = user_id {
            pk_sender_id
        } else if is_webhook || message.author.bot {
            // If the message is from an unknown webhook, let them know we can't help, and
            // then exit
            message.reply(ctx,
                include_str!("../messages/errors/messaging_bot.txt"),
            ).await?;

            return Ok(())
        } else {
            // If the message is from a normal user, go ahead as normal
            message.author.id
        };

        let task_dm_user = async {
            user_id.create_dm_channel(ctx).await?.say(ctx, format!(
                include_str!("../messages/cmds/create/dm_01.txt"),
                channel_id=message.channel_id,
                guild_name=guild_name.as_ref(),
            )).await
        };

        let task_message_user = message.channel_id.say(ctx, format!(
            include_str!("../messages/cmds/create/dm_sent.txt"),
            at=message.author,
        ));

        let results = join!(task_message_user, task_dm_user);
        results.0?;
        results.1?;
        Ok(())
    }

    /// Prints a small help message including available pronouns and modifier.
    ///
    /// Specifically, the message will contain a list of pronouns (bases) in the
    /// provided [`PronounDatabase`], the provided modifiers, a list of common
    /// commands, a brief description of a common "workflow", and a short
    /// license statement.  The latter three can be found in the `/messages`
    /// directory of the source repository.  Removing the license statement
    /// would be a violation of the AGPL license that you received this program
    /// under, so don't do that, although you can modify it within reason or if
    /// you clone or re-license the repository.
    pub async fn small_help<I: IntoIterator<Item=S>, S: Display>(
        ctx: &Context,
        channel: ChannelId,
        pronouns: &PronounDatabase,
        modifiers: I,
    ) -> serenity::Result<()> {
        let mut modifiers = modifiers.into_iter().peekable();
        let sample_modifier = modifiers.peek()
            .map(|m| m.to_string())
            .expect("At least one modifier must be used");
        channel.send_message(ctx, |message| {
            message.embed(|e| {
                e
                .field("Available Pronouns",
                    pronouns.keys()
                        .sorted()
                        .join("\n"),
                    true,
                )
                .field("Modifiers",
                    modifiers
                        .join("\n"),
                    true,
                )
                .field(
                    "Help",
                    format!(
                        include_str!("../messages/cmds/help/basic.txt"),
                        sample_modifier=sample_modifier,
                    ),
                    true,
                )
                .field(
                    "Common Commands",
                    include_str!("../messages/cmds/help/commands.txt"),
                    true,
                )
                .footer(|f|
                    f.text(include!("../messages/cmds/help/license.rs"))
                )
                .color(Colour::from_rgb(91, 206, 250))
            })
        }).await?;
        Ok(())
    }

    /// Sends a user the full help guide
    ///
    /// The help guide sent to the user can be found in the `/messages` folder
    /// of the source repository, named `full_help_p<n>.txt`, where n is the
    /// part number between 1 and 3.  The third part of the help guide includes
    /// an invite link to add this bot to another server.  Generating this link
    /// requires an HTTP call the first time this method is used, but not any
    /// following time, thanks to the [`get_invite`] method.
    ///
    /// If a [`ChannelId`] is provided in the `channel` argument, then a message
    /// is printed to that channel informing the user that they've been sent a
    /// DM.
    ///
    /// [`get_invite`]: PronounBot::get_invite
    pub async fn big_help(
        &self,
        ctx: &Context,
        user: &User,
        channel: Option<ChannelId>,
    ) -> serenity::Result<()> {
        let task_reply_message = channel.map(|channel|
            channel.say(ctx, format!(
                include_str!("../messages/cmds/help/dm_sent.txt"),
                at=user,
            ))
        );

        let invite = Self::get_invite(ctx).await?;

        let task_send_help = async {
            user.direct_message(ctx, |f| f.content(
                include_str!("../messages/cmds/help/full/part_1.txt"),
            )).await?;
            user.direct_message(ctx, |f| f.content(
                format!(
                    include_str!("../messages/cmds/help/full/part_2.txt"),
                    sample_modifier=self.modifiers.get(0).expect("Need at least one modifier"),
                ),
            )).await?;
            user.direct_message(ctx, |f| f.content(
                include_str!("../messages/cmds/help/full/part_3.txt"),
            )).await?;
            user.direct_message(ctx, |f| f.content(
                format!(
                    include_str!("../messages/cmds/help/full/part_4.txt"),
                    invite,
                )
            )).await
        };

        if let Some(task_reply_message) = task_reply_message {
            let results = join!(task_reply_message, task_send_help);
            results.0?;
            results.1?;
        } else {
            task_send_help.await?;
        }
        Ok(())
    }

    /// A wrapper around [`Pronouner::big_help`] with checks for bot messages, and PK users
    pub async fn help_cmd(
        &self,
        ctx: &Context,
        message: &Message,
    ) -> serenity::Result<()> {
        let user_id;
        #[cfg(feature = "pluralkit")] {
            // If the feature is enabled, try to look up the PK sender
            let pk_info = Self::is_pk_message(message).await;
            user_id = pk_info.map(|pkmsg| pkmsg.sender);
        }
        #[cfg(not(feature = "pluralkit"))] {
            // If pluralkit support is not enabled, fail automatically
            user_id = None;
        }

        let user_id = if let Some(pk_sender_id) = user_id {
            // If the user is using pluralkit, message the system account
            pk_sender_id
        } else if message.author.bot {
            // We can't message bots, so just stop now
            message.reply(ctx,
                include_str!("../messages/errors/messaging_bot.txt"),
            ).await?;

            return Ok(())
        } else {
            // If the message is from a normal user, go ahead as normal
            message.author.id
        };

        self.big_help(ctx, &user_id.to_user(ctx).await?, Some(message.channel_id)).await
    }

    /// A wrapper around [`transition`](PronounBot::transition) with permission
    /// check and responses.
    ///
    /// Used for the `!pronouns transition` command
    ///
    /// If this command is run with no arguments, then a message will be printed
    /// describing some information about how the hierarchy must be set up in
    /// order to preform the transition.  The user will then be prompted to
    /// double check the hierarchy and rerun the command with the `confirm`
    /// argument.
    ///
    /// If the author of the provided message has permission to manage roles
    /// (i.e. do what the bot is doing for them), then they are notified that
    /// they've started transitioning the server, and a typing indicator is sent
    /// until the completion notice is sent.
    /// A response  will be sent if no [`serenity::Error`] occurs.  A successful
    /// message is sent even if the user has not [`PronounRole`]s.  There is no
    /// error message that can be sent.
    ///
    /// Alternatively, they are notified that they lack permissions.
    ///
    /// The `bot_user` argument is necessary to preform hierarchy checks, and
    /// should be the user id of the bot itself.
    ///
    /// For more details regarding the behavior of this method, please see
    /// [`PronounBot::transition`], the underlying method.
    pub async fn transition_cmd(
        &self,
        http: impl AsRef<Http>,
        message: &Message,
        guild: &mut Guild,
        pronouns: &mut PronounDatabase,
        args: &[&str],
        bot_user: UserId,
    ) -> serenity::Result<()> {
        if !(args.is_empty() && args[0].eq_ignore_ascii_case("confirm")) {
            message.channel_id.say(http.as_ref(), include_str!("../messages/cmds/transition/confirm.txt")).await?;
        } else if guild.member_permissions(&message.author.id).manage_roles() {

            let (result_announce_start, result_start_typing) = join!(
                message.channel_id.say(http.as_ref(), include_str!("../messages/cmds/transition/start.txt")),
                message.channel_id.broadcast_typing(http.as_ref()),
            );
            result_announce_start?;
            result_start_typing?;

            let http_ref = http.as_ref();
            self.transition(http.as_ref(), guild, pronouns, bot_user, |role, to, skip| {
                let role_name = role.name.clone();

                async move {
                    if skip {
                        message.channel_id.send_message(http_ref, move|m| m.embed(|e| e
                            .color(Colour::from_rgb(226, 66, 63))
                            .description(
                                format!(include_str!("../messages/cmds/transition/skip.txt"),
                                    from=role_name,
                                    to=to,
                                )
                            )
                        )).await?;
                    } else {
                        message.channel_id.say(http_ref,
                            format!(include_str!("../messages/cmds/transition/match.txt"),
                                from=role_name,
                                to=to,
                            ),
                        ).await?;
                    };
                    Ok(())
                }
            }).await?;
            message.channel_id.say(http.as_ref(), include_str!("../messages/cmds/transition/finish.txt")).await?;
        } else {
            message.channel_id.say(http.as_ref(), include_str!("../messages/cmds/transition/perms.txt")).await?;
        }
        Ok(())
    }

    /// Convert existing roles on a server to be compatable with the bot's
    /// syntax.
    ///
    /// This is able to recognize any pronouns in a three form pattern, even
    /// with modifiers in front of them.  Any recognized pronouns that aren't
    /// already recognized by the provided database are registered into the
    /// database.  In addition, any roles that seem to indicate a pronoun are
    /// renamed to the full form of that pronoun.  For example, `trying she/her`
    /// becomes `Trying She/Her/Hers` provided that `She/Her/Hers` was already
    /// registered with the database, or if there was another role that
    /// contained the full pronoun `she/her/hers` (case insensitvely)
    ///
    /// This will also be able to recognize special pronouns through case
    /// insensitivity and accepting roles that are a slice of a special.  For
    /// example, `any` might become `Any Pronouns`.  However, this only
    /// recognizes special pronouns that the `PronounBot` was created with.
    ///
    /// The `callback` argument is called for each role that is processed.  The
    /// first argument is the role being processed, still with its old name.
    /// The second argument is the name that the role is being renamed to.  The
    /// third argument is true if the bot is prevented due to a hierarchy
    /// problem from actually making any changes to that role.
    ///
    /// If the bot recognizes that a role has no members and can be deleted
    /// without forgetting about the pronoun set it represents, then the bot may
    /// prune the role.  This only occurs if the role would have otherwise been
    /// deleted by the bot's natural pruning, and is not guarenteed to happen in
    /// any case.
    ///
    /// The `bot_user` argument is necessary to preform hierarchy checks, and
    /// should be the user id of the bot itself.
    ///
    /// Yes this is a joke about transitioning.
    pub async fn transition<FR>(
        &self,
        http: impl AsRef<Http>,
        guild: &mut Guild,
        pronouns: &mut PronounDatabase,
        bot_user: UserId,
        mut callback: impl FnMut(&Role, String, bool) -> FR,
    ) -> serenity::Result<()>
    where
        FR: Future<Output = serenity::Result<()>> ,
    {
        // All roles that are already associated with a pronoun.  We can skip
        // over these while parsing
        let existing_prs: HashSet<RoleId> = pronouns.list_roles()
            .map(|r| r.role.id)
            .collect();

        // We need our position to check role hierarchies
        let our_position = guild.members.get(&bot_user)
            .ok_or(serenity::Error::Model(ModelError::ItemMissing))?
            .roles
            .iter()
            .map(|rid| guild.roles.get(rid).unwrap().position)
            .max()
            .unwrap_or(0);

        // The negation of the previous set, all of the roles we need to pay
        // attention to
        let unlisted_roles = guild.roles.values_mut()
            .filter(|r| !existing_prs.contains(&r.id))
            .collect_vec();

        // First, check for any new pronoun sets that need to be created
        for role in &unlisted_roles {
            lazy_static! {
                static ref THREE_FORM: Regex = regex::RegexBuilder::new(
                    "([a-z']{1,5})/([a-z']{1,5})/([a-z']{1,5})"
                ).case_insensitive(true).build().unwrap();
            }

            if let Some(capture) = THREE_FORM.captures(&role.name) {
                let role_name = Self::role_name_from_iter(
                    capture.iter().skip(1).filter_map(|m| m.map(|m| m.as_str()))
                );
                pronouns.add_roleless_pronoun(role_name);
            }
        }

        // Next, start converting over the roles into the database
        let guild_id = guild.id;
        let callbacks_tasks = FuturesUnordered::new();
        let role_delete_tasks = FuturesUnordered::new();
        for role in unlisted_roles {
            // Check if this can be matched with a pronoun set
            if let Some((set, modif)) = self.parse_args(
                pronouns,
                &role.name.split_ascii_whitespace().collect_vec()
            ).last() { // Use last() instead of next() to drop the Iterator and free up db
                let role_name = role_name_from_components(set.base(), &modif);

                let mut members_with_role = guild.members.values_mut()
                        .filter(|m| m.roles.contains(&role.id));

                if role.position >= our_position {

                    // Can't do anything with the role 'cause hierarchies
                    callbacks_tasks.push((callback)(role, role_name, true));
                    continue;

                } else if let Some(existing_role) = set.get(&modif) {

                    // A role for this already exists; Merge them
                    // TODO: Stop making so many HTTP requests here

                    members_with_role
                        .map(|m| m.add_role(http.as_ref(), existing_role.id))
                        .collect::<FuturesUnordered<_>>()
                        .try_collect()
                        .await?;
                    // Cannot postpone adding the role, since it modifies Members

                    role_delete_tasks.push(guild_id.delete_role(http.as_ref(), role.id));

                } else {

                    // Rename this role, or trim if possible
                    if
                        !set.is_empty() &&
                        members_with_role.next().is_none()
                    {
                        // Trim eligable
                        role_delete_tasks.push(guild_id.delete_role(http.as_ref(), role.id));
                    } else {
                        // Not trime eligable, so rename
                        let role = guild_id.edit_role(
                            http.as_ref(),
                            role.id,
                            |e| e.name(&role_name),
                        ).await?;
                        // Cannot postpone renaming the role, since it's needed to update
                        // the pronoun database

                        pronouns.add_pr(PronounRole::new(role, modif));
                    }
                }

                callbacks_tasks.push((callback)(role, role_name, false));
            };
        }

        let (callbacks_result, role_delete_result) = join!(
            callbacks_tasks.try_collect(),
            role_delete_tasks.try_collect(),
        );

        callbacks_result?;
        role_delete_result
    }

    /// Print out some stats about the pronoun usage on the server.
    pub async fn stats_cmd(
        &self,
        ctx: &Context,
        channel: ChannelId,
        pronouns: &PronounDatabase,
    ) -> serenity::Result<()> {
        let mut stats = pronouns.calc_stats(ctx)
            .await?
            .user_counts(pronouns)
            .into_iter()
            .filter(|p| p.1 > 0)
            .collect_vec();

        stats.sort_unstable_by(|a, b| b.1.cmp(&a.1));

        let stats_str = stats.iter()
            .map(|d| format!("**{}** \u{2015} {}", d.0, d.1))
            .join("\n");

        channel.send_message(ctx, |b| b
            .embed(|e| e
                .color(Colour::from_rgb(245, 169, 184))
                .title("Pronoun Usage Stats")
                .description(stats_str)
            )
        ).await?;

        Ok(())
    }

    /// Attempt to prune all roles in the guild
    ///
    /// This attempts to remove any roles which are eligable for pruning.  A
    /// message is printed in the given channel indicating exactly how many were
    /// pruned.  This needs no arguments, and you probably shouldn't even limit
    /// who can call it, since there's really not much downside to using it.
    /// I'm really tired right now, I can't write good documentation.
    pub async fn prune_cmd(
        &self,
        ctx: &Context,
        pronouns: &PronounDatabase,
        channel_id: ChannelId,
    ) -> serenity::Result<()> {
        let (_, count) = self.try_prune(
            ctx,
            pronouns,
            pronouns.list_roles(),
            false,
        ).await?;

        channel_id.say(ctx, format!(include_str!("../messages/cmds/prune.txt"),
            count = count,
        )).await?;

        Ok(())
    }

    /// A utility method for making parse-compatable names from an iterable of
    /// pronoun forms.
    ///
    /// The first three forms are removed from the iterable, and are converted
    /// to title case before being joined by a "/".
    fn role_name_from_iter<I,S>(iter: I) -> String
    where
        I: IntoIterator<Item=S>,
        S: AsRef<str>
    {
        iter
            .into_iter()
            .take(3)
            .map(|s| s.as_ref().to_lowercase())
            .map(|mut s| {
                let c = s.remove(0).to_ascii_uppercase();
                s.insert(0, c);
                s
            })
            .join("/")
    }

    /// A helpful method that produces and caches  an invite link.
    ///
    /// Normally, generating an invite link requires an HTTP request each time
    /// you do it.  Now, it doesn't have to!  (request is stored in ctx.data)
    pub async fn get_invite(ctx: &Context) -> serenity::Result<String> {
        struct InviteTracker { }
        impl TypeMapKey for InviteTracker {
            type Value = String;
        }

        let mut share_map = ctx.data.write().await;
        if let Some(invite) = share_map.get::<InviteTracker>() {
            Ok(invite.to_string())
        } else {
            let invite = ctx.cache
                .current_user()
                .await
                .invite_url(ctx, REQUIRED_PERMS)
                .await?;
            share_map.insert::<InviteTracker>(invite.clone());
            Ok(invite)
        }
    }

    /// The main message handling method
    ///
    /// A brief rundown on how the message methods work, and why there's so many
    /// of them.  A request comes in to the [`Framework::dispatch`] method,
    /// which starts [`message_noerr`] in a separate thread.  [`message_noerr`]
    /// is designed specifically for this purpose, which rolls up the
    /// [`message_raw`] method (which can return an error) and the error handler
    /// method [`handle_error`] into one neat package.
    ///
    /// So in short, [`Framework::dispatch`] exists to dispatch threads,
    /// [`message_noerr`] exists to deal with errors, [`message_raw`] is where the
    /// message handling take place, and [`handle_error` is where the error
    /// handling takes place.
    ///
    /// [`Framework::dispatch`]: serenity::framework::Framework::dispatch
    /// [`message_noerr`]: PronounBot::message_noerr
    /// [`message_raw`]: PronounBot::message_raw
    /// [`handle_error`]: PronounBot::handle_error
    async fn message_raw(
        &self,
        ctx: &Context,
        message: &Message
    ) -> serenity::Result<()> {
        if PREFIX_REGEX.is_match(&message.content) {
            if let Some(mut guild) = message.guild(ctx).await {

                let bot_user =  ctx.cache.current_user_id().await;

                debug!("Handling command from {}: {}",
                    message.author,
                    message.content,
                );

                // Double check perms
                let our_permissions = guild.user_permissions_in(
                    message.channel_id,
                    bot_user,
                );
                let perm_difference = REQUIRED_PERMS - our_permissions;
                if !perm_difference.is_empty() {
                    return Err(serenity::Error::Model(
                        ModelError::InvalidPermissions(perm_difference)
                    ))
                }

                // Form pronoun database
                let mut pronouns = PronounDatabase::with_roles(
                    guild.roles
                         .values(),
                    self.specials.iter()
                );

                // Double check heirarchy
                let our_position = guild.member(ctx, bot_user).await?
                    .roles(ctx)
                    .await
                    .unwrap()
                    .iter()
                    .map(|r| r.position)
                    .max();
                let pronouns_position = pronouns.list_roles()
                    .map(|r| r.role.position)
                    .max();
                if pronouns_position > our_position {
                    return Err(serenity::Error::Model(
                        ModelError::Hierarchy
                    ))
                }

                // Parse args
                let breakdown: Vec<&str> = message.content
                    .split_ascii_whitespace()
                    .collect();

                if breakdown.len() >= 2 {
                    match breakdown[1].to_ascii_lowercase().as_str() {
                        "remove" => {
                            self.remove_pronouns_cmd(
                                ctx,
                                &breakdown[2..],
                                message,
                                &pronouns,
                            ).await?;
                        },
                        "add" => {
                            self.add_pronouns_cmd(
                                ctx,
                                &breakdown[2..],
                                message,
                                &pronouns,
                            ).await?;
                        },
                        "clear" => {
                            self.clear_pronouns_cmd(
                                ctx,
                                message,
                                &pronouns,
                            ).await?;
                        },
                        "create" => {
                            Self::start_create_cmd(
                                ctx,
                                message,
                                &guild.name,
                            ).await?;
                        },
                        "help" => {
                            self.help_cmd(
                                ctx,
                                message,
                            ).await?;
                        },
                        "transition" => {
                            self.transition_cmd(
                                ctx,
                                message,
                                &mut guild,
                                &mut pronouns,
                                &breakdown[2..],
                                bot_user,
                            ).await?;
                        },
                        "stats" => {
                            self.stats_cmd(
                                ctx,
                                message.channel_id,
                                &pronouns,
                            ).await?;
                        },
                        "prune" => {
                            self.prune_cmd(
                                ctx,
                                &pronouns,
                                message.channel_id
                            ).await?;
                        },
                        _ => {
                            self.add_pronouns_cmd(
                                ctx,
                                &breakdown[1..],
                                message,
                                &pronouns,
                            ).await?;
                        },
                    };
                } else {
                    Self::small_help(
                        ctx,
                        message.channel_id,
                        &pronouns,
                        self.modifiers.iter(),
                    ).await?;
                }
            } else if
                PREFIX_REGEX.is_match(&message.content) &&
                message.content.ends_with("help")
            {
                self.big_help(
                    ctx,
                    &message.author,
                    None,
                ).await?;
            } else {
                message.channel_id.say(ctx, include_str!("../messages/welcome.txt")).await?;
            }
        } else if !message.is_own(ctx).await {
            if let Private(dm) = message.channel_id.to_channel(ctx).await? {
               self.handle_dm(ctx, dm).await?;
            }
        }
        Ok(())
    }

    /// A messaging function with error handling
    ///
    /// Please see the [`PronounBot::message_raw`] docs for a description of the
    /// path that incoming messages take to be handled.
    async fn message_noerr(
        &self,
        ctx: Context,
        message: Message
    ) {
        let mut result = self.message_raw(&ctx, &message).await;
        while let Err(error) = result {
            result = self.handle_error(&ctx, message.channel_id, error).await;
        }
    }

    /// An error handler that returns more errors to be handled
    ///
    /// Please see the [`PronounBot::message_raw`] docs for a description of the
    /// path that incoming messages take to be handled.
    async fn handle_error(
        &self,
        ctx: &Context,
        channel: ChannelId,
        err: serenity::Error,
    ) -> serenity::Result<()> {
        let error_msg: Option<&'static str> = match err {
            Model(err) => match err {
                ModelError::InvalidPermissions(p) =>
                    if !p.send_messages() { //ie We CAN send messages
                        Some(
                            include_str!("../messages/errors/perms.txt")
                        )
                    } else {
                        //TODO: Maybe a channel blacklist or smth?
                        None
                    },
                ModelError::Hierarchy => Some(
                    include_str!("../messages/errors/heirarchy.txt")
                ),
                ModelError::EmbedTooLarge(_) => Some(
                    include_str!("../messages/errors/large_embed.txt")
                ),
                ModelError::MessagingBot => Some(
                    include_str!("../messages/errors/messaging_bot.txt")
                ),
                _ => {
                    warn!("Unexpected Model Error:\n{:#?}", err);
                    Some(
                        include_str!("../messages/errors/unknown.txt")
                    )
                }
            },
            Http(err) => {
                warn!("Yikes!  HTTP Error:\n{:#?}", err);
                None
            },
            _ => {
                error!("!!!!! A completely unknown error!\n{:#?}", err);
                None
            },
        };

        if let Some(msg) = error_msg {
            channel.say(ctx, msg).await?;
        }
        Ok(())
    }

    #[cfg(feature = "pluralkit")]
    /// Attempt to determine is a message was sent through PluralKit
    ///
    /// This message is essentially a wrapper around
    /// [`pluralkit::PkMessage::query_message()`] with a few notable changes.  This method
    /// includes built in error handling, and will log a warning is an unexpected error
    /// occurs, and simply return an [`Option`] if a member was found successfully.
    /// Additionally, this method performs a check to see if the provided message is a
    /// webhook, meaning for most messages, this is an inexpensive opperation, and it *is*
    /// safe to use this method to check if a message was sent by pluralkit.
    ///
    /// This method does make external HTTP requests.
    async fn is_pk_message(message: &Message) -> Option<PkMessage> {
        let is_webhook = message.author.discriminator == 0;
        if !is_webhook {
            return None;
        }

        match PkMessage::query_message(message.id).await {
            Ok(msg) => Some(msg),
            Err(e) => {
                match e {
                    PkError::NotFound => {},
                    _ => {
                        warn!(
                            "An error occured in PluralKit ({}:{})\n{}",
                            file!(),
                            line!(),
                            e
                        );
                    }
                };
                None
            },
        }
    }

    /// A method for DRYing out code by performing some common functions for PK users
    ///
    /// This method performs several checks, including handling and responding for some
    /// warning or errors that might come up (within scope, ofc).
    ///
    /// 1. Check if the user is a PK user, and if so, let them know that we have limited
    ///    ability to change their pronouns, and what other options might be, but
    ///    successfully return the [`Member`] corresponding to their system's Discord
    ///    account, so that processing can go ahead with that.
    ///
    /// 2. Check if the message came from an unknown webhook.  If this is the case,
    ///    there's not much we can do, so exit successfully, but without returning a
    ///    member to work on.
    ///
    /// 3. If the message did not come from a webhook, just look up the User's [`Member`],
    ///    and return that successfully.
    ///
    /// When using this method, first understand exactly what it's doing, then use it in
    /// place of [`Pronouner::get_member`] (which is actually used internally).  Unwrap
    /// the result using the `?` syntax and then check if the result is None.  If so,
    /// exit, but otherwise proceed using the returned [`Member`].
    ///
    /// Errors can occur:
    ///
    ///  * When sending a reply message to the user
    ///  * When looking up the user in the cache
    async fn generic_pk_checks(ctx: &Context, message: &Message) -> serenity::Result<Option<Member>> {
        // Collect information about where this message came from
        let is_webhook = message.webhook_id.is_some();

        #[cfg(feature = "pluralkit")] {
            let pk_info = Self::is_pk_message(message).await;
            if let Some(pkmsg) = pk_info {
                // If the user is using PluralKit, look them up and give them a rundown on
                // what the bot can and can't do, then try to go ahead using the system's
                // account.

                // Look up member
                let member = message.guild(ctx)
                                    .await
                                    .unwrap()
                                    .member(ctx, pkmsg.sender)
                                    .await?;

                message.channel_id.say(ctx, format!(
                    include_str!("../messages/errors/pluralkit_generic.txt"),
                    member_name=pkmsg.member.name.clone().unwrap(), // Auto falls back to display name
                    system_account=member,
                )).await?;

                return Ok(Some(member))
            }
        }

        if is_webhook {
            // If the message is from an unknown webhook, let them know we can't help, and
            // then exit
            message.reply(ctx,
                include_str!("../messages/errors/webhook.txt"),
            ).await?;

            Ok(None)
        } else {
            // If the message is from a normal user, go ahead as normal
            Self::get_member(ctx, message).await.map(Some)
        }
    }


    /// A function that can conveniently fit the `create_with` argument of
    /// `get_or_create` methods.
    ///
    /// Probably needs a lambda or smth tho
    async fn create_role(
        guild: GuildId,
        ctx: &Context,
        name: impl ToString,
        model: Option<&Role>
    ) -> serenity::Result<Role> {
        guild
            .create_role(
                ctx,
                |r| {
                    let r = r.name(name);
                    if let Some(role) = model {
                        r.colour(role.colour.0.into())
                        .position((role.position).try_into().unwrap())
                    } else {
                        r
                    }
                }
            ).await
    }

    /// The main handler for received DMs
    ///
    /// Currently, this just deals with responses used in creating a new pronoun
    /// base (i.e. the `!pronouns create`).  It's assumed that the received DM
    /// is not sent by this bot.
    ///
    /// DMs are handled as followed:
    ///
    /// - First, a DM is sent to the user with a header starting with a square
    ///   bracket (`[`) followed by a single character.  The character denotes
    ///   the current sequence.  Currently recognized sequences are listed below
    ///   square brackets, and instructions on what to send.
    /// - Each time a DM is received, we search backwards for the last sent
    ///   header message, denoted by an opening bracket.
    /// - A list of the users messages is then passed on to the appropriate
    ///   sequence method (ending with `_seq`).  Based on these messages, the
    ///   current state and appropraite response can be determined.
    ///
    /// For example, if the user is sent a message saying "[a..] Please enter the
    /// first form of the pronoun" (this isn't the actual message), and then
    /// responds "Jee/Jem/Jeir" followed shortly by "Jee", the bot would be able
    /// to determine that (based on the `a`) the user is currently in a pronoun
    /// creation sequence, and that one valid message has been received.  The
    /// appropriate response after one valid message has been received is
    /// "Please enter the second for of the pronoun" (not actually).  And thus
    /// the command proceeds.
    ///
    /// This somewhat convoluted strategy allows the bot to not locally store
    /// any state (other than immutable config stuff), which allows it to easily
    /// recover from a crash and saves memory, disk space, and lines of code.
    ///
    /// Currently recognized sequences:
    ///  - `a` **-** Pronoun creation sequence, started `!pronouns create
    ///  - `C` **-** Sequence termination, e.g. on sequence cancel.  Indicates that
    ///     the bot should stop scanning upwards.
    pub async fn handle_dm(&self, ctx: &Context, dm: PrivateChannel) -> serenity::Result<()> {

        // Build up some context
        //TODO: Replace this with a real iterator
        let mut all_messages = dm
            .messages(ctx, |builder| builder)
            .await?
            .into_iter()
            .peekable();
        let our_id = ctx.cache.current_user_id().await;
        let user_messages = all_messages
            .peeking_take_while(
                |m| !(m.author.id == our_id && m.content.starts_with('['))
            )
            .filter(|m| m.author.id != our_id)
            .map(|m| m.content.to_lowercase())
            .collect::<Vec<String>>();
        let header_message = if let Some(header) = all_messages.peek() {
            header.content.clone()
        } else {
            return Ok(())
        };

        let last_message = &user_messages.get(0)
            .expect("handle_dm called without a DM being received");
        if last_message.eq_ignore_ascii_case("cancel") {
            dm.say(ctx,
                include_str!("../messages/cmds/cancel.txt")
            ).await?;
            return Ok(())
        }

        match &header_message[1..2] {
            "a" => self.create_seq(
                ctx,
                dm,
                user_messages,
                &header_message,
            ).await,
            "C" => Ok(()),
            _ => panic!("Unknown DM code received!"),
        }
    }

    /// Handle the pronoun creation sequence specifically.
    ///
    /// Requires all user-sent messages prior to receiving the header message,
    /// whose content should start with the header [a followed by the 20
    /// character ChannelId.  See [`PronounBot::handle_dm`] for more information
    /// on how this works
    ///
    /// State is currently determined by the number of *valid* messages that
    /// have been received, i.e. the number of messages that fit the
    /// `VALID_FORM` regex.  In addition, the method checks if the last message
    /// (if present) was valid in order to determine if an error message should
    /// be sent instead of a normal prompt.
    ///
    /// Upon receiving the third valid message, i.e. when the pronoun
    /// construction is complete, the bot will lookup the channel referenced in
    /// the introductory message and the corresponding guild, to which a role is
    /// created and an info message is sent.
    async fn create_seq(
        &self,
        ctx: &Context,
        dm: PrivateChannel,
        messages: Vec<String>,
        header: &str,
    ) -> serenity::Result<()> {

        lazy_static! {
            static ref VALID_FORM: Regex = Regex::new(
                r"^[a-z']{1,5}$"
            ).unwrap();
        }

        let mut valid_messages = messages
            .iter()
            .filter(|s| VALID_FORM.is_match(s))
            .collect::<Vec<&String>>();

        valid_messages.reverse();

        const RESPONSES: [&str; 3] = [
            include_str!("../messages/cmds/create/dm_02.txt"),
            include_str!("../messages/cmds/create/dm_03.txt"),
            include_str!("../messages/cmds/create/dm_04.txt"),
        ];

        match valid_messages.len().cmp(&3) {
            Ordering::Less => {
                let response = if VALID_FORM.is_match(messages.get(0).unwrap()) {
                    RESPONSES[valid_messages.len()]
                } else {
                    include_str!("../messages/cmds/create/input_error.txt")
                };
                dm.say(ctx, response).await?;
            },
            Ordering::Equal => {
                // Rebuild context
                let channel_id = ChannelId(header[2..22].parse().unwrap());
                let channel = match channel_id.to_channel(ctx).await? {
                    Guild(gc) => gc,
                    _ => panic!(format!(
                            "Corrupt message sent by us?!?!\nMessage: {}",
                            header,
                        )),
                };

                let guild_id = channel.guild_id;
                let try_guild = guild_id.to_guild_cached(ctx).await;
                let try_partial;
                let roles;
                if let Some(guild) = try_guild.as_ref() {
                    roles = guild.roles.values();
                } else {
                    try_partial = guild_id.to_partial_guild(ctx).await?.roles;
                    roles = try_partial.values();
                }
                let pronouns = PronounDatabase::with_roles(roles, &self.specials);

                // Start doing the rank
                let rank_name = Self::role_name_from_iter(valid_messages);

                let response;
                if !pronouns.contains_key(&rank_name) {
                    // Pronouns don't exist yet: Create them

                    Self::create_role(
                        guild_id,
                        ctx,
                        &rank_name,
                        pronouns.list_roles().next().map(|p| p.role).as_ref()
                    ).await?;

                    response = format!(
                        include_str!("../messages/cmds/create/success.txt"),
                        pronouns=rank_name,
                        sample_modifier=self.modifiers.get(0).unwrap(),
                    );

                    channel.say(ctx, format!(
                        include_str!("../messages/cmds/create/notif.txt"),
                        at=dm.recipient,
                        pronouns=rank_name,
                    )).await?;
                } else {
                    // Pronouns already exists

                    response = format!(
                        include_str!("../messages/cmds/create/exist.txt"),
                        pronouns=rank_name,
                        sample_modifier=self.modifiers.get(0).unwrap(),
                    );
                }
                dm.say(ctx, response).await?;
            },
            Ordering::Greater => panic!("Unexpectedly long sequence!"),
        }

        Ok(())
    }
}

#[async_trait]
impl EventHandler for PronounBot {
    async fn ready(&self, ctx:Context, data: Ready) {
        let gids = data.guilds
            .iter()
            .map(GuildStatus::id);

        ctx.shard.chunk_guilds(gids, None, None, None);

        info!("Bot online!  Logged in as {}", data.user.name);


        if let Ok(invite) = PronounBot::get_invite(&ctx).await {
            println!("\
===== Invite me! =====
{}
======================", invite);
        } else {
            warn!("Problem creating an invite :(");
        }

        ctx.set_presence(
            Some(include!("../messages/presence.rs")),
            OnlineStatus::Online,
        ).await;
    }

    async fn guild_create(&self, _ctx: Context, guild: Guild, _is_new: bool) {
        info!("Connected to guild: {}", guild.name);
    }
}

#[async_trait]
impl serenity::framework::Framework for PronounBot {
    /// Message handling with thread dispatching
    ///
    /// Please see the [`PronounBot::message_raw`] docs for a description of the
    /// path that incoming messages take to be handled.
    async fn dispatch(
        &self,
        ctx: Context,
        msg: Message,
    ) {
        if
            (
                msg.is_private() ||
                PREFIX_REGEX.is_match(&msg.content)
            ) &&
            !msg.is_own(&ctx).await
        {
            self.message_noerr(ctx, msg).await;
        }
    }
}
